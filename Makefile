fips-update:
	./fips update

setup:
	./fips setup emscripten

build:
	./fips build

gen:
	./fips gen

run:
	./fips run

br: build run

emsc-release:
	./fips set config sapp-webgl2-emsc-ninja-release
	./fips set target pirkle-sapp
	./fips clean
	./fips gen

macos-debug:
	./fips set config sapp-metal-osx-make-debug
	./fips set target pirkle-sapp
	./fips clean
	./fips gen
