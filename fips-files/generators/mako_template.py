from mako.runtime import Context
from mako.template import Template
import yaml

def generate(in_path, out_path_source, out_path_header, *args):
    print('Rendering mako ...')
    with open(in_path, 'r') as fh:
        yaml_data = yaml.load(fh)
    template_path = (
        ('template_source', out_path_source),
        ('template_header', out_path_header),
    )
    for template_key, out_path in template_path:
        template = Template(yaml_data[template_key])
        with open(out_path, 'w') as fh:
            ctx = Context(fh, **yaml_data['data'])
            template.render_context(ctx)
