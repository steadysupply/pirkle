#include <stdio.h>

#include "microui/microui.h"
#include "microui/atlas.inl"

#include "ringbuff/ringbuff.h"

#include "sokol_app.h"
#include "sokol_gfx.h"
#include "sokol_gl.h"

#include "generated/ui_piano.h"

char logbuf_data[500];
char tmp[3];
ringbuff_t logbuf;
static   int logbuf_updated = 0;

void ui_init() {
  ringbuff_init(&logbuf, logbuf_data, sizeof(logbuf_data));
}

void ui_frame(void) {
  for (int index = 0; index < UI_PIANO_NUM_KEYS; index++) {
    if (ui_piano_pressed[index] && !ui_piano_pressed_previously[index]) {
      ui_piano_pressed_previously[index] = 1;
    } else if (!ui_piano_pressed[index] && ui_piano_pressed_previously[index]) {
      ui_piano_pressed_previously[index] = 0;
    }
  }
  ringbuff_write(&logbuf, "1\n", 3);
  ringbuff_write(&logbuf, "0\n", 3);
  logbuf_updated = 1;
}

void ui_piano_keyup(int keycode) {
  for (int index = 0; index < UI_PIANO_NUM_KEYS; index++) {
    if (keycode == ui_piano_keymap[index]) {
      ui_piano_pressed_previously[index] = 1;
      ui_piano_pressed[index] = 0;
    }
  }
}

void ui_piano_keydown(int keycode) {
  for (int index = 0; index < UI_PIANO_NUM_KEYS; index++) {
    if (keycode == ui_piano_keymap[index]) {
      ui_piano_pressed[index] = 1;
      ui_piano_pressed_previously[index] = 0;
    }
  }
}

static void log_window(mu_Context *ctx) {
  static mu_Container window;

  /* init window manually so we can set its position and size */
  if (!window.inited) {
    mu_init_window(ctx, &window, 0);
    window.rect = mu_rect(350, 40, 300, 200);
  }

  if (mu_begin_window(ctx, &window, "Log Window")) {

    /* output text panel */
    static mu_Container panel;
    mu_layout_row(ctx, 1, (int[]) { -1 }, -28);
    mu_begin_panel(ctx, &panel);
    int row_spec[UI_PIANO_NUM_KEYS];
    for (int index = 0; index < UI_PIANO_NUM_KEYS; index++) {
      row_spec[index] = 10;
    }
    mu_layout_row(ctx, 1, row_spec, 10);
    for (int index = 0; index < UI_PIANO_NUM_KEYS; index++) {
      ringbuff_read(&logbuf, tmp, sizeof(tmp));
      mu_text(ctx, tmp);
    }
    mu_end_panel(ctx);
    if (logbuf_updated) {
      // panel.scroll.y = panel.content_size.y;
      logbuf_updated = 0;
    }

    mu_end_window(ctx);
  }
}

void ui(mu_Context* ctx) {
  ui_piano(ctx);
  log_window(ctx);
}

/*== micorui renderer =========================================================*/
static sg_image atlas_img;
static sgl_pipeline pip;

void r_init(void) {

    /* atlas image data is in atlas.inl file, this only contains alpha 
       values, need to expand this to RGBA8
    */
    uint32_t rgba8_size = ATLAS_WIDTH * ATLAS_HEIGHT * 4;
    uint32_t* rgba8_pixels = (uint32_t*) malloc(rgba8_size);
    for (int y = 0; y < ATLAS_HEIGHT; y++) {
        for (int x = 0; x < ATLAS_WIDTH; x++) {
            uint32_t index = y*ATLAS_WIDTH + x;
            rgba8_pixels[index] = 0x00FFFFFF | ((uint32_t)atlas_texture[index]<<24);
        }
    }
    atlas_img = sg_make_image(&(sg_image_desc){
        .width = ATLAS_WIDTH,
        .height = ATLAS_HEIGHT,
        /* LINEAR would be better for text quality in HighDPI, but the
           atlas texture is "leaking" from neighbouring pixels unfortunately
        */
        .min_filter = SG_FILTER_NEAREST,
        .mag_filter = SG_FILTER_NEAREST,
        .content = {
            .subimage[0][0] = {
                .ptr = rgba8_pixels,
                .size = rgba8_size
            }
        }
    });
    pip = sgl_make_pipeline(&(sg_pipeline_desc){
        .blend = {
            .enabled = true,
            .src_factor_rgb = SG_BLENDFACTOR_SRC_ALPHA,
            .dst_factor_rgb = SG_BLENDFACTOR_ONE_MINUS_SRC_ALPHA
        }
    });

    free(rgba8_pixels);
}

void r_begin(int disp_width, int disp_height) {
    sgl_defaults();
    sgl_push_pipeline();
    sgl_load_pipeline(pip);
    sgl_enable_texture();
    sgl_texture(atlas_img);
    sgl_matrix_mode_projection();
    sgl_push_matrix();
    sgl_ortho(0.0f, (float) disp_width, (float) disp_height, 0.0f, -1.0f, +1.0f);
    sgl_begin_quads();
}

void r_end(void) {
    sgl_end();
    sgl_pop_matrix();
    sgl_pop_pipeline();
}

void r_draw(void) {
    sgl_draw();
}

void r_push_quad(mu_Rect dst, mu_Rect src, mu_Color color) {
    float u0 = (float) src.x / (float) ATLAS_WIDTH;
    float v0 = (float) src.y / (float) ATLAS_HEIGHT;
    float u1 = (float) (src.x + src.w) / (float) ATLAS_WIDTH;
    float v1 = (float) (src.y + src.h) / (float) ATLAS_HEIGHT;

    float x0 = (float) dst.x;
    float y0 = (float) dst.y;
    float x1 = (float) (dst.x + dst.w);
    float y1 = (float) (dst.y + dst.h);

    sgl_c4b(color.r, color.g, color.b, color.a);
    sgl_v2f_t2f(x0, y0, u0, v0);
    sgl_v2f_t2f(x1, y0, u1, v0);
    sgl_v2f_t2f(x1, y1, u1, v1);
    sgl_v2f_t2f(x0, y1, u0, v1);
}

void r_draw_rect(mu_Rect rect, mu_Color color) {
    r_push_quad(rect, atlas[ATLAS_WHITE], color);
}

void r_draw_text(const char* text, mu_Vec2 pos, mu_Color color) {
    mu_Rect dst = { pos.x, pos.y, 0, 0 };
    for (const char* p = text; *p; p++) {
        mu_Rect src = atlas[ATLAS_FONT + (unsigned char)*p];
        dst.w = src.w;
        dst.h = src.h;
        r_push_quad(dst, src, color);
        dst.x += dst.w;
    }
}

void r_draw_icon(int id, mu_Rect rect, mu_Color color) {
    mu_Rect src = atlas[id];
    int x = rect.x + (rect.w - src.w) / 2;
    int y = rect.y + (rect.h - src.h) / 2;
    r_push_quad(mu_rect(x, y, src.w, src.h), src, color);
}

int r_get_text_width(const char* text, int len) {
    int res = 0;
    for (const char* p = text; *p && len--; p++) {
        res += atlas[ATLAS_FONT + (unsigned char)*p].w;
    }
    return res;
}

int r_get_text_height(void) {
    return 18;
}

void r_set_clip_rect(mu_Rect rect) {
    sgl_end();
    sgl_scissor_rect(rect.x, rect.y, rect.w, rect.h, true);
    sgl_begin_quads();
}

