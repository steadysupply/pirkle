#include <math.h>
#include <stdio.h>
#include "sokol_audio.h"

// #include "generated/ui_globals.h"
#include "generated/ui_piano.h"

#define NUM_SAMPLES (32)
static int sample_pos;
static float samples[NUM_SAMPLES];

void osc_render(int sample_rate) {
  /*
  // count how many keys pressed
  int keys_pressed = 0;
  for (int index = 0; index < UI_PIANO_NUM_KEYS; index++) {
    keys_pressed += ui_piano_pressed[index];
  }
  float key_amp = 1.0 / (float)keys_pressed;
  */
  int num_frames = saudio_expect();
  float sample;
  for (int frame_idx = 0; frame_idx < num_frames; frame_idx++) {

    sample = 0; // reset sample

    // add up pitched results for keys currently pressed
    for (int key_idx = 0; key_idx < UI_PIANO_NUM_KEYS; key_idx++) {
      if (!ui_piano_pressed[key_idx]) continue;
      float phase = ui_piano_key_phases[key_idx];
      float tri = (phase * 2 / M_PI);
      if (phase < 0) {
        tri = 1.0 + tri;
      } else {
        tri = 1.0 - tri;
      }
      phase += ui_piano_key_frequency[key_idx] / (float)sample_rate;
      if (phase >= M_PI) {
        phase = phase - (2 * M_PI);
      }
      ui_piano_key_phases[key_idx] = phase;
      sample += tri;
    }
    // printf("%f\n", sample);
    samples[sample_pos++] = sample; // * key_amp;
    if (sample_pos == NUM_SAMPLES) {
      sample_pos = 0;
      saudio_push(samples, NUM_SAMPLES);
    }
  }
}
