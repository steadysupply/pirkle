#include "sokol_gfx.h"
#include "sokol_app.h"
#include "microui/microui.h"
#define SOKOL_GL_IMPL
#include "sokol_gl.h"
#include "cdbgui/cdbgui.h"
#include <stdio.h> /* sprintf */

#include "sokol_audio.h"

#include "ui.h"
#include "osc.h"

static float bg[3] = { 90.0f, 95.0f, 100.0f }; 


static mu_Context mu_ctx;


/* callbacks */
static int text_width_cb(mu_Font font, const char* text, int len) {
    if (len == -1) {
        len = (int) strlen(text);
    }
    return r_get_text_width(text, len);
}

static int text_height_cb(mu_Font font) {
    return r_get_text_height();
}

/* initialization */
static void init(void) {
    /* setup sokol-gfx */
    saudio_setup(&(saudio_desc){0});
    sg_setup(&(sg_desc){
        .gl_force_gles2 = sapp_gles2(),
        .mtl_device = sapp_metal_get_device(),
        .mtl_renderpass_descriptor_cb = sapp_metal_get_renderpass_descriptor,
        .mtl_drawable_cb = sapp_metal_get_drawable,
        .d3d11_device = sapp_d3d11_get_device(),
        .d3d11_device_context = sapp_d3d11_get_device_context(),
        .d3d11_render_target_view_cb = sapp_d3d11_get_render_target_view,
        .d3d11_depth_stencil_view_cb = sapp_d3d11_get_depth_stencil_view,
    });
    // __cdbgui_setup(1);

    /* setup sokol-gl */
    sgl_setup(&(sgl_desc_t){0});

    /* setup microui renderer */
    r_init();

    /* setup microui */
    mu_init(&mu_ctx);
    mu_ctx.text_width = text_width_cb;
    mu_ctx.text_height = text_height_cb;
    ui_init();
}

static const char key_map[512] = {
    [SAPP_KEYCODE_LEFT_SHIFT]       = MU_KEY_SHIFT,
    [SAPP_KEYCODE_RIGHT_SHIFT]      = MU_KEY_SHIFT,
    [SAPP_KEYCODE_LEFT_CONTROL]     = MU_KEY_CTRL,
    [SAPP_KEYCODE_RIGHT_CONTROL]    = MU_KEY_CTRL,
    [SAPP_KEYCODE_LEFT_ALT]         = MU_KEY_ALT,
    [SAPP_KEYCODE_RIGHT_ALT]        = MU_KEY_ALT,
    [SAPP_KEYCODE_ENTER]            = MU_KEY_RETURN,
    [SAPP_KEYCODE_BACKSPACE]        = MU_KEY_BACKSPACE,
};

static void event(const sapp_event* ev) {
    /* FIXME: need to filter out events consumed by the Dear ImGui debug UI */
    // __cdbgui_event(ev);
    switch (ev->type) {
        case SAPP_EVENTTYPE_MOUSE_DOWN:
            mu_input_mousedown(&mu_ctx, (int)ev->mouse_x, (int)ev->mouse_y, (1<<ev->mouse_button));
            break;
        case SAPP_EVENTTYPE_MOUSE_UP:
            mu_input_mouseup(&mu_ctx, (int)ev->mouse_x, (int)ev->mouse_y, (1<<ev->mouse_button));
            break;
        case SAPP_EVENTTYPE_MOUSE_MOVE:
            mu_input_mousemove(&mu_ctx, (int)ev->mouse_x, (int)ev->mouse_y);
            break;
        case SAPP_EVENTTYPE_MOUSE_SCROLL:
            mu_input_scroll(&mu_ctx, (int)ev->scroll_x, (int)ev->scroll_y);
            break;
        case SAPP_EVENTTYPE_KEY_DOWN:
            ui_piano_keydown(ev->key_code);
            break;
        case SAPP_EVENTTYPE_KEY_UP:
            ui_piano_keyup(ev->key_code);
            break;
/*
        case SAPP_EVENTTYPE_CHAR:
            {
                char txt[2] = { ev->char_code & 255, 0 };
                mu_input_text(&mu_ctx, txt);
            }
            break;
*/
        default:
            break;
    }
}

/* do one frame */
void frame(void) {

    /* UI definition */
    mu_begin(&mu_ctx);
    // TODO: should this happen in ui.c?
    ui(&mu_ctx);
    mu_end(&mu_ctx);

    /* micro-ui rendering */
    r_begin(sapp_width(), sapp_height());
    mu_Command* cmd = 0;
    while(mu_next_command(&mu_ctx, &cmd)) {
        switch (cmd->type) {
            case MU_COMMAND_TEXT:
                r_draw_text(cmd->text.str, cmd->text.pos, cmd->text.color);
                break;
            case MU_COMMAND_RECT:
                r_draw_rect(cmd->rect.rect, cmd->rect.color);
                break;
            case MU_COMMAND_ICON:
                r_draw_icon(cmd->icon.id, cmd->icon.rect, cmd->icon.color);
                break;
            case MU_COMMAND_CLIP:
                r_set_clip_rect(cmd->clip.rect);
                break;
        }
    }
    r_end();
    
    /* render the sokol-gfx default pass */
    sg_begin_default_pass(&(sg_pass_action){
            .colors[0] = {
                .action = SG_ACTION_CLEAR,
                .val = { bg[0] / 255.0f, bg[1] / 255.0f, bg[2] / 255.0f, 1.0f }
            }
        }, sapp_width(), sapp_height());

    r_draw();
    // __cdbgui_draw();
    osc_render(saudio_sample_rate());
    sg_end_pass();
    sg_commit();
    ui_frame();
}

static void cleanup(void) {
    // __cdbgui_shutdown();
    sgl_shutdown();
    saudio_shutdown();
    sg_shutdown();
}

sapp_desc sokol_main(int argc, char* argv[]) {
    return (sapp_desc){
        .init_cb = init,
        .frame_cb = frame,
        .cleanup_cb = cleanup,
        .event_cb = event,
        .width = 720,
        .height = 540,
        .gl_force_gles2 = true,
        .window_title = "pirkle"
    };
}
