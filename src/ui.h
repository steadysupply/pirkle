// ui functions
void ui_init(void);
void ui_frame(void);
void ui(mu_Context*);
void ui_piano_keyup(int);
void ui_piano_keydown(int);

// renderer functions
void r_init(void);
void r_begin(int disp_width, int disp_height);
void r_end(void);
void r_draw(void);
void r_push_quad(mu_Rect dst, mu_Rect src, mu_Color color);
void r_draw_rect(mu_Rect rect, mu_Color color);
void r_draw_text(const char* text, mu_Vec2 pos, mu_Color color);
void r_draw_icon(int id, mu_Rect rect, mu_Color color);
int r_get_text_width(const char* text, int len);
int r_get_text_height(void);
void r_set_clip_rect(mu_Rect rect);
